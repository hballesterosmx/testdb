package com.tata;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
/**
 * Hello world!
 *
 */
public class App 
{
	private static Connection conn = null;
	private static String usuario = "USERTATA";
	private static String pass = "1234567";
	private static String url = "jdbc:oracle:thin:@localhost:1521:xe";
	
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
		Connection con = getConnection();
		String query = "SELECT * FROM EMPELADO";
		Statement stmt;
		try{
			stmt = con.createStatement();
			ResultSet response = stmt.executeQuery(query);
			
			while(response.next()){
				String nombre = response.getString("NOMBRE");
				String edad = response.getString("EDAD");
				String depto = response.getString("DEPTO");
				String antiguedad = response.getString("ANTIGUEDAD");
				
				System.out.println("Resultado Nombre:" + nombre + " Edad: " + edad + " Depto: " + depto + " Antiguedad: " + antiguedad);
			}
			
		}catch(Exception e){
			System.out.println("Exception...." + e);
			e.printStackTrace();
		}
    }
	
	public static Connection getConnection(){
		
		try{
			Class.forName("oracle.jdbc.OracleDriver");
			conn= DriverManager.getConnection(url, usuario, pass);
			if(conn != null){
				System.out.println("Conexión exitosa");
			}
		}catch(ClassNotFoundException e1){
			System.out.println("Conexión incorrecta");
			e1.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
		return conn;
	}
}
